package ibs.microservices.news;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewsMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewsMicroserviceApplication.class, args);
	}

}
