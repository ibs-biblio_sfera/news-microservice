package ibs.microservices.news.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Calendar;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NewsDto {
    private UUID id;
    private String header;
    private String article;
    private boolean isPinned;
    private boolean isHide;
    private String photoUrl;
    private Calendar creationDateTime;
    private Calendar modifyDateTime;
}
