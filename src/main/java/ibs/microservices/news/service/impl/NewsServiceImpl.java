package ibs.microservices.news.service.impl;

import ibs.microservices.news.dto.NewsDto;
import ibs.microservices.news.entity.News;
import ibs.microservices.news.exception.ResourceNotFoundException;
import ibs.microservices.news.mapper.NewsMapper;
import ibs.microservices.news.repository.NewsRepository;
import ibs.microservices.news.service.NewsService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class NewsServiceImpl implements NewsService {

    private NewsRepository newsRepository;

    @Override
    public NewsDto createNews(NewsDto newsDto) {

        News news = NewsMapper.mapToNews(newsDto);
        News savedNews = newsRepository.save(news);
        return NewsMapper.mapToNewsDto(savedNews);
    }

    @Override
    public NewsDto getNewsById(UUID newsId) {
        News news = newsRepository.findById(newsId)
                .orElseThrow(() ->
                        new ResourceNotFoundException("Новость с таким Id не найдена " + newsId));
        return NewsMapper.mapToNewsDto(news);
    }

    @Override
    public List<NewsDto> getAllNews() {
        List<News> news = newsRepository.findAll();
        return news.stream().map(NewsMapper::mapToNewsDto)
                .collect(Collectors.toList());
    }

    @Override
    public NewsDto updateNews(UUID newsId, NewsDto updatedNews) {

        News news = newsRepository.findById(newsId)
                .orElseThrow(() ->
                        new ResourceNotFoundException("Новость с таким Id не найдена " + newsId));

        news.setArticle(updatedNews.getArticle());
        news.setHeader(updatedNews.getHeader());
        news.setPinned(updatedNews.isPinned());
        news.setHide(updatedNews.isHide());
        news.setPhotoUrl(updatedNews.getPhotoUrl());
        news.setModifyDateTime(Calendar.getInstance());

        News updatedNewsObj = newsRepository.save(news);

        return NewsMapper.mapToNewsDto(updatedNewsObj);
    }

    @Override
    public void deleteNews(UUID newsId) {
        News news = newsRepository.findById(newsId)
                .orElseThrow(() ->
                        new ResourceNotFoundException("Новость с таким Id не найдена " + newsId));
        newsRepository.deleteById(newsId);

    }
}
