package ibs.microservices.news.service;

import ibs.microservices.news.dto.NewsDto;

import java.util.List;
import java.util.UUID;

public interface NewsService {
    NewsDto createNews(NewsDto newsDto);

    NewsDto getNewsById(UUID newsId);

    List<NewsDto> getAllNews();

    NewsDto updateNews(UUID newsId, NewsDto updatedNews);

    void deleteNews(UUID newsId);
}
