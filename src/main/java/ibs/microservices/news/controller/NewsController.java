package ibs.microservices.news.controller;

import ibs.microservices.news.dto.NewsDto;
import ibs.microservices.news.service.NewsService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/news")
public class NewsController {

    private NewsService newsService;

    @PostMapping
    public ResponseEntity<NewsDto> createnews(@RequestBody NewsDto newsDto) {
        NewsDto savedNews = newsService.createNews(newsDto);
        return new ResponseEntity<>(savedNews, HttpStatus.CREATED);
    }

    @GetMapping("{id}")
    public ResponseEntity<NewsDto> getNewsById(@PathVariable("id") UUID newsId) {
        NewsDto newsDto = newsService.getNewsById(newsId);
        return ResponseEntity.ok(newsDto);
    }

    @GetMapping
    public ResponseEntity<List<NewsDto>> getAllNews(){
        List<NewsDto> news = newsService.getAllNews();
        return ResponseEntity.ok(news);
    }

    @PutMapping("{id}")
    public ResponseEntity<NewsDto> updateNews(@PathVariable("id") UUID newsId,
                                              @RequestBody NewsDto updatedNews) {
        NewsDto newsDto = newsService.updateNews(newsId, updatedNews);
        return ResponseEntity.ok(newsDto);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteNews(@PathVariable("id") UUID newsId) {
        newsService.deleteNews(newsId);
        return ResponseEntity.ok("Успешно удалено !");
    }
}
