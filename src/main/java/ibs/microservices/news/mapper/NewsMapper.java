package ibs.microservices.news.mapper;

import ibs.microservices.news.dto.NewsDto;
import ibs.microservices.news.entity.News;

public class NewsMapper {
    public static NewsDto mapToNewsDto(News news) {
        return new NewsDto(
                news.getId(),
                news.getHeader(),
                news.getArticle(),
                news.isPinned(),
                news.isHide(),
                news.getPhotoUrl(),
                news.getCreationDateTime(),
                news.getModifyDateTime()
        );
    }

    public static News mapToNews(NewsDto newsDto){
        return new News(
                newsDto.getId(),
                newsDto.getHeader(),
                newsDto.getArticle(),
                newsDto.isPinned(),
                newsDto.isHide(),
                newsDto.getPhotoUrl(),
                newsDto.getCreationDateTime(),
                newsDto.getModifyDateTime()
        );
    }
}
