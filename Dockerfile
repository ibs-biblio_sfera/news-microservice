FROM openjdk

ENV APP_HOME /app

RUN mkdir $APP_HOME

WORKDIR $APP_HOME

COPY . $APP_HOME

RUN ["./mvnw", "dependency:go-offline"]

COPY ./ $APP_HOME

RUN ["./mvnw", "package", "-DskipTests"]

CMD ["java", "-jar", "target/news-microservice-0.0.1-SNAPSHOT.jar"]
